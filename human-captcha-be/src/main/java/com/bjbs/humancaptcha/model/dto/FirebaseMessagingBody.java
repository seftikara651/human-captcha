package com.bjbs.humancaptcha.model.dto;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class FirebaseMessagingBody {
  private String title;
  private String body;
  private Map<String, String> data;
}