package com.bjbs.humancaptcha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HumanCaptchaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HumanCaptchaApplication.class, args);
	}

}
