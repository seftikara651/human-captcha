package com.bjbs.humancaptcha.configuration;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableScheduling
public class ApplicationConfiguration {

  private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfiguration.class);

  @Bean
  @Transactional
  public CommandLineRunner init() {
    return args -> {

    };
  }

  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    return builder.build();
  }

  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }

  @Bean
  public ObjectMapper objectMapper() {
    return new ObjectMapper();
  }

  @Bean
  FirebaseMessaging firebaseMessaging() throws IOException {
    try {
      return FirebaseMessaging.getInstance(FirebaseApp.getInstance());
    } catch (IllegalStateException err) {
      LOGGER.warn("{}: Creating new one!", err.getMessage());

      // @formatter:off
      return FirebaseMessaging.getInstance(
        FirebaseApp.initializeApp(
          FirebaseOptions.builder()
            .setCredentials(
              GoogleCredentials.fromStream(
                new ClassPathResource("firebase/credential.json")
                  .getInputStream()))
            .build()));
      // @formatter:on
    }
  }

}