package com.bjbs.humancaptcha.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // @formatter:off
    http
      .authorizeRequests()
        .antMatchers("/**").permitAll()
        .anyRequest().authenticated()
      .and().sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
        .sessionFixation().changeSessionId()
        .sessionConcurrency(session ->
          session
            .maximumSessions(1)
            .maxSessionsPreventsLogin(false)
        )
      .and().csrf()
        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
    ;
    // @formatter:on
  }

}