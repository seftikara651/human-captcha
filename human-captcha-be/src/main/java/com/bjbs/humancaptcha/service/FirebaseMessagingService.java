package com.bjbs.humancaptcha.service;

import com.bjbs.humancaptcha.model.dto.FirebaseMessagingBody;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FirebaseMessagingService {

  private static final Logger LOGGER = LoggerFactory.getLogger(FirebaseMessagingService.class);

  private final FirebaseMessaging firebaseMessaging;

  @Autowired
  public FirebaseMessagingService(FirebaseMessaging firebaseMessaging) {
    this.firebaseMessaging = firebaseMessaging;
  }

  public String sendNotification(String token, FirebaseMessagingBody body) {
    try {
      // @formatter:off
      return firebaseMessaging.send(
        Message.builder()
          .setToken(token)
          .setNotification(
            Notification.builder()
              .setTitle(body.getTitle())
              .setBody(body.getBody())
              .build())
          .putAllData(body.getData())
          .build());
      // @formatter:on
    } catch (FirebaseMessagingException e) {
      LOGGER.debug("Failed to send firebase notification!", e);

      return "ERR: " + e.getMessage();
    }
  }

}